<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends  CI_Controller{

    public function index(){
        $this->load->view("index");
    }

    public function showServices(){
        $this->load->view("services");
    }

    public function book(){
        $this->load->view("book");
    }

}
