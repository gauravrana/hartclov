<?php $this->load->view('common');?>
<?php $this->load->view('layout');?>
<!-- Contact Section -->
    <section id="book">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Enter Your Details</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <form name="book" id="bookingDetails" novalidate>
                        <div class="row">
                            <div class="col-md-offset-7 col-md-5">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Your Name *" id="name" required data-validation-required-message="Please enter your name.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <select id="gender" name="gender" class="form-control form-dropdown">
                                        <option value="female">Female</option>
                                        <option value="male">Male</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Your Email *" id="email" required data-validation-required-message="Please enter your email address.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="tel" class="form-control" placeholder="Your Phone *" id="phone" required data-validation-required-message="Please enter your phone number.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Your Address *" id="address" required data-validation-required-message="Please enter a message."></textarea>
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="tel" class="form-control" placeholder="Pincode *" id="pincode" required data-validation-required-message="Please enter your pincode.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <select id="day" name="day" class="form-control form-dropdown">
                                        <option value="today">Today</option>
                                        <option value="tomorrow">Tomorrow</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select id="timeslot" name="timeslot" class="form-control form-dropdown">
                                        <option value="11">10 am to 11 am</option>
                                        <option value="12">11 am to 12 pm</option>
                                        <option value="13">12 pm to 1 pm</option>
                                        <option value="14">1 pm to 2 pm</option>
                                        <option value="15">2 pm to 3 pm</option>
                                        <option value="16">3 pm to 4 pm</option>
                                        <option value="17">4 pm to 5 pm</option>
                                        <option value="18">5 pm to 6 pm</option>
                                        <option value="19">6 pm to 7 pm</option>
                                        <option value="20">7 pm to 8 pm</option>
                                    </select>
                                </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 text-center">
                                <div id="success"></div>
                                <button type="submit" class="btn btn-xl">Book</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
<?php $this->load->view('footer');?>