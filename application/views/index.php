<?php $this->load->view('common');?>
<?php $this->load->view('homenav');?>
    <!-- Header -->
    <header>
        <div class="container">
            <div class="intro-text">
                <div class="intro-lead-in">Welcome!</div>
                <div class="intro-heading">It's Nice To Meet You</div>
                <a href="/services" class="col-md-offset-4 col-sm-4 page-scroll btn btn-xl">Book</a>
            </div>
        </div>
    </header>

    <!-- Services Grid Section -->
    <section id="services" class="bg-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">services</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6 services-item">
                    <a href="#faceServicesModal" class="services-link" data-toggle="modal">
                        <div class="services-hover">
                            <div class="services-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="/assets/img/services/facial_services.jpg" class="img-responsive" alt="" style="height:265px;width:425px;">
                    </a>
                    <div class="services-caption">
                        <h4>Facial Services</h4>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 services-item">
                    <a href="#bodyServicesModal" class="services-link" data-toggle="modal">
                        <div class="services-hover">
                            <div class="services-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="/assets/img/services/waxing_services.jpg" class="img-responsive" alt="" style="height:265px;width:425px;">
                    </a>
                    <div class="services-caption">
                        <h4>Body Services</h4>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 services-item">
                    <a href="#hairServicesModal" class="services-link" data-toggle="modal">
                        <div class="services-hover">
                            <div class="services-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="/assets/img/services/hair_services.jpg" class="img-responsive" alt="" style="height:265px;width:425px;">
                    </a>
                    <div class="services-caption">
                        <h4>Hair Services</h4>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 services-item">
                    <a href="#handsAndFeetServicesModal" class="services-link" data-toggle="modal">
                        <div class="services-hover">
                            <div class="services-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="/assets/img/services/hands_feet_services.jpg" class="img-responsive" alt="" style="height:265px;width:425px;">
                    </a>
                    <div class="services-caption">
                        <h4>Hands & Feet Services</h4>
                    </div>
                </div>
                <a href="/services" class="col-md-offset-4 col-sm-4 page-scroll btn btn-xl">Book</a>
            </div>
        </div>
    </section>

    <!-- How It Works Section -->
    <section id="howItWorks">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">How It Works</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <ul class="timeline">
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="/assets/img/details.png" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4 class="subheading">Select the services</h4>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="/assets/img/user.png" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4 class="subheading">Enter your details</h4>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="/assets/img/appointment.png" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4 class="subheading">Book your appointment</h4>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <h4>Sit back
                                    <br>&
                                    <br> stay glad!</h4>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!-- Clients Aside -->
    <aside class="clients" style="display:none">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <a href="#">
                        <img src="/assets/img/logos/envato.jpg" class="img-responsive img-centered" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-sm-6">
                    <a href="#">
                        <img src="/assets/img/logos/designmodo.jpg" class="img-responsive img-centered" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-sm-6">
                    <a href="#">
                        <img src="/assets/img/logos/themeforest.jpg" class="img-responsive img-centered" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-sm-6">
                    <a href="#">
                        <img src="/assets/img/logos/creative-market.jpg" class="img-responsive img-centered" alt="">
                    </a>
                </div>
            </div>
        </div>
    </aside>

<?php $this->load->view('footer');?>
    <!-- services Modals -->
    <!-- Use the modals below to showcase details howItWorks your services projects! -->

    <!-- services Modal 1 -->
    <div class="services-modal modal fade" id="faceServicesModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <h2>Face Services</h2>
                            <div class="row">
                                <div class="col-md-6 col-sm-6 services-item">
                                    <ul class="list-group">
                                        <li class="list-group-item active">Facial</li>
                                        <li class="list-group-item"><span class="badge">Rs900</span>Anti Acne Facial</li>
                                        <li class="list-group-item"><span class="badge">Rs200</span>Basic Clean Up</li>
                                        <li class="list-group-item"><span class="badge">Rs700</span>Fruit Facial</li>
                                        <li class="list-group-item"><span class="badge">Rs600</span>Anti Tan Facial</li>
                                        <li class="list-group-item"><span class="badge">Rs400</span>Classic Facial</li>
                                        <li class="list-group-item"><span class="badge">Rs1000</span>Insta Glow Facial</li>
                                        <li class="list-group-item"><span class="badge">Rs800</span>Skin Tightening Facial</li>
                                    </ul>
                                </div>
                                <div class="col-md-6 col-sm-6 services-item">
                                    <ul class="list-group">
                                        <li class="list-group-item active">Threading</li>
                                        <li class="list-group-item"><span class="badge">Rs25</span>Chin</li>
                                        <li class="list-group-item"><span class="badge">Rs25</span>Forehead</li>
                                        <li class="list-group-item"><span class="badge">Rs60</span>Jawline</li>
                                        <li class="list-group-item"><span class="badge">Rs40</span>Eyebrow</li>
                                        <li class="list-group-item"><span class="badge">Rs160</span>Full Face</li>
                                        <li class="list-group-item"><span class="badge">Rs25</span>Lower Lip</li>
                                        <li class="list-group-item"><span class="badge">Rs60</span>Neck</li>
                                        <li class="list-group-item"><span class="badge">Rs60</span>Side Locks</li>
                                        <li class="list-group-item"><span class="badge">Rs30</span>Upper Lip</li>
                                    </ul>
                                </div>
                            </div>
                            <a href="/services" class="col-md-offset-4 col-sm-4 page-scroll btn btn-xl">Book</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- services Modal 2 -->
    <div class="services-modal modal fade" id="bodyServicesModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <h2>Body Services</h2>
                            <div class="col-md-6 col-sm-6 services-item">
                                <ul class="list-group">
                                    <li class="list-group-item active">Waxing</li>
                                    <li class="list-group-item"><span class="badge">Rs25</span>Bikini Line</li>
                                    <li class="list-group-item"><span class="badge">Rs25</span>Chin</li>
                                    <li class="list-group-item"><span class="badge">Rs60</span>Full Arms</li>
                                    <li class="list-group-item"><span class="badge">Rs40</span>Buttock</li>
                                    <li class="list-group-item"><span class="badge">Rs160</span>Full Back</li>
                                    <li class="list-group-item"><span class="badge">Rs25</span>Face</li>
                                    <li class="list-group-item"><span class="badge">Rs60</span>Full Front</li>
                                    <li class="list-group-item"><span class="badge">Rs60</span>Full Legs</li>
                                    <li class="list-group-item"><span class="badge">Rs30</span>Half Arms</li>
                                </ul>
                            </div>
                            <div class="col-md-6 col-sm-6 services-item">
                                <ul class="list-group">
                                    <li class="list-group-item active">Massage</li>
                                    <li class="list-group-item"><span class="badge">Rs300</span>Back & Shoulder Massage</li>
                                    <li class="list-group-item"><span class="badge">Rs450</span>Foot Reflexology</li>
                                    <li class="list-group-item"><span class="badge">Rs300</span>Foot Massage</li>
                                    <li class="list-group-item"><span class="badge">Rs300</span>Hand Massage</li>
                                    <li class="list-group-item"><span class="badge">Rs160</span>Head Massage</li>
                                </ul>
                            </div>
                            <div class="col-md-6 col-sm-6 services-item">
                                <ul class="list-group">
                                    <li class="list-group-item active">Body Polish</li>
                                    <li class="list-group-item"><span class="badge">Rs500</span>Full Back</li>
                                    <li class="list-group-item"><span class="badge">Rs450</span>Full Hands</li>
                                    <li class="list-group-item"><span class="badge">Rs1300</span>Full Body</li>
                                    <li class="list-group-item"><span class="badge">Rs600</span>Full Legs</li>
                                </ul>
                            </div>
                            <a href="/services" class="col-md-offset-4 col-sm-4 page-scroll btn btn-xl">Book</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- services Modal 3 -->
    <div class="services-modal modal fade" id="hairServicesModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2>Hair Services</h2>
                            <div class="col-md-6 col-sm-6 services-item">
                                <ul class="list-group">
                                    <li class="list-group-item active">Hair Spa</li>
                                    <li class="list-group-item"><span class="badge">Rs525</span>Hair Nourishment</li>
                                    <li class="list-group-item"><span class="badge">Rs725</span>L`Oreal Hydrating Spa</li>
                                    <li class="list-group-item"><span class="badge">Rs660</span>L`Oreal Nourishing Spa</li>
                                </ul>
                            </div>
                            <div class="col-md-6 col-sm-6 services-item">
                                <ul class="list-group">
                                    <li class="list-group-item active">Hair Coloring</li>
                                    <li class="list-group-item">
                                        <ul class="list-group">
                                            <li class="list-group-item active">Global Color</li>
                                            <li class="list-group-item"><span class="badge">Rs600</span>Application Only</li>
                                            <li class="list-group-item"><span class="badge">Rs600</span>Upto Elbow</li>
                                            <li class="list-group-item"><span class="badge">Rs600</span>Upto Shoulder</li>
                                        </ul>
                                    </li>
                                    <li class="list-group-item"><span class="badge">Rs25</span>Henna Conditioning</li>
                                    <li class="list-group-item"><span class="badge">Rs60</span>Loreal Crown Highlights</li>
                                    <li class="list-group-item"><span class="badge">Rs40</span>Henna Coloring</li>
                                    <li class="list-group-item"><span class="badge">Rs160</span>INOA Global Color</li>
                                    <li class="list-group-item"><span class="badge">Rs25</span>Loreal Global Color & Crown Highlights</li>
                                </ul>
                            </div>
                            <a href="/services" class="col-md-offset-4 col-sm-4 page-scroll btn btn-xl">Book</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- services Modal 4 -->
    <div class="services-modal modal fade" id="handsAndFeetServicesModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2>Hands & Feet Services</h2>
                            <div class="col-md-6 col-sm-6 services-item">
                                <ul class="list-group">
                                    <li class="list-group-item active">Hair Spa</li>
                                    <li class="list-group-item"><span class="badge">Rs525</span>Express Manicure</li>
                                    <li class="list-group-item"><span class="badge">Rs725</span>Express Pedicure</li>
                                    <li class="list-group-item"><span class="badge">Rs660</span>Foot Massage</li>
                                </ul>
                            </div>
                            <a href="/services" class="col-md-offset-4 col-sm-4 page-scroll btn btn-xl">Book</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


