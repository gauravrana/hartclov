<?php $this->load->view('common');?>
<?php $this->load->view('layout');?>
<div id="bookingPage" >
    <div class="container" style="padding: 150px 0px;">
        <div class="row">
            <div class="col-md-6 col-sm-6 services-item">
                <section id="services">
                    <div class="container">
                        <div class="row">
                            <div class="panel-group col-md-6" id="accordion">
                                <div class="panel panel-default row">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" aria-expanded="true" href="#threading" class="collapsed">
                                            <h4 class="panel-title bold">
                                                Threading
                                            </h4>
                                        </a>
                                    </div>
                                    <div id="threading" class="panel-collapse collapse" style="height: 0px;">
                                        <div class="panel-body">
                                            <div class="col-lg-6 col-sm-12 col-xs-6">
                                                <label class="checkbox"><input type="checkbox" value="">Eyebrow - Rs 40</label>
                                                <label class="checkbox"><input type="checkbox" value="">Chin - Rs 25</label>
                                                <label class="checkbox"><input type="checkbox" value="">Upper Lip - Rs 30</label>
                                                <label class="checkbox"><input type="checkbox" value="">Lower Lip - Rs 25</label>
                                            </div>
                                            <div class="col-lg-6 col-sm-12 col-xs-6">
                                                <label class="checkbox"><input type="checkbox" value="">Forehead - Rs 25</label>
                                                <label class="checkbox"><input type="checkbox" value="">Side Locks - Rs 60</label>
                                                <label class="checkbox"><input type="checkbox" value="">Jawline - Rs 60</label>
                                                <label class="checkbox"><input type="checkbox" value="">Neck - Rs 60</label>
                                                <label class="checkbox"><input type="checkbox" value="">Full Face - Rs 150</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default row">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#face" class="collapsed">
                                            <h4 class="panel-title bold">
                                                Face
                                            </h4>
                                        </a>
                                    </div>
                                    <div id="face" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="col-lg-6 col-sm-12 col-xs-6">
                                                <div class="text-center">Facial:</div>
                                                <label class="checkbox"><input type="checkbox" value="" data-type="Facial">Anti Tan - Rs 600</label>
                                                <label class="checkbox"><input type="checkbox" value="" data-type="Facial">Skin Tightening - Rs 800</label>
                                                <label class="checkbox"><input type="checkbox" value="" data-type="Facial">Insta Glow - Rs 1000</label>
                                                <label class="checkbox"><input type="checkbox" value="" data-type="Facial">Fruit Facial - Rs 700</label>
                                                <label class="checkbox"><input type="checkbox" value="" data-type="Facial">Classic - Rs 400</label>
                                            </div>
                                            <div class="col-lg-6 col-sm-12 col-xs-6">
                                                <div class="text-center">Clean Up:</div>
                                                <label class="checkbox"><input type="checkbox" value="" data-type="Clean Up">Basic - Rs 200</label>
                                                <label class="checkbox"><input type="checkbox" value="" data-type="Clean Up">Anti Acne - Rs 250</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default row">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#hand-feet" class="collapsed">
                                            <h4 class="panel-title bold">
                                                Hands and Feet
                                            </h4>
                                        </a>
                                    </div>
                                    <div id="hand-feet" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="col-lg-6 col-sm-12 col-xs-6">
                                                <div class="text-center">Pedicure:</div>
                                                <label class="checkbox"><input type="checkbox" value="" data-type="Pedicure">Spa - Rs 350</label>
                                                <label class="checkbox"><input type="checkbox" value="" data-type="Pedicure">French - Rs 400</label>
                                                <label class="checkbox"><input type="checkbox" value="" data-type="Pedicure">Paraffin - Rs 600</label>
                                                <label class="checkbox"><input type="checkbox" value="" data-type="Pedicure">Mineral - Rs 800</label>
                                                <label class="checkbox"><input type="checkbox" value="" data-type="Pedicure">Cut File and Polish - Rs 100</label>
                                                <label class="checkbox"><input type="checkbox" value="" data-type="Pedicure">Cut File and Polish: French - Rs 150</label>
                                            </div>
                                            <div class="col-lg-6 col-sm-12 col-xs-6">
                                                <div class="text-center">Manicure:</div>
                                                <label class="checkbox"><input type="checkbox" value="" data-type="Manicure">Spa - Rs 200</label>
                                                <label class="checkbox"><input type="checkbox" value="" data-type="Manicure">French - Rs 300</label>
                                                <label class="checkbox"><input type="checkbox" value="" data-type="Manicure">Paraffin - Rs 350</label>
                                                <label class="checkbox"><input type="checkbox" value="" data-type="Manicure">Mineral - Rs 550</label>
                                                <label class="checkbox"><input type="checkbox" value="" data-type="Manicure">Cut File and Polish - Rs 100</label>
                                                <label class="checkbox"><input type="checkbox" value="" data-type="Manicure">Cut File and Polish: French - Rs 150</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-md-6 col-sm-6 services-item">
                <section id="book" >
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <form name="book" id="bookingDetails" novalidate>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Your Name *" id="name" required data-validation-required-message="Please enter your name.">
                                                <p class="help-block text-danger"></p>
                                            </div>
                                            <div class="form-group">
                                                <select id="gender" name="gender" class="form-control form-dropdown">
                                                    <option value="female">Female</option>
                                                    <option value="male">Male</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <input type="email" class="form-control" placeholder="Your Email *" id="email" required data-validation-required-message="Please enter your email address.">
                                                <p class="help-block text-danger"></p>
                                            </div>
                                            <div class="form-group">
                                                <input type="tel" class="form-control" placeholder="Your Phone *" id="phone" required data-validation-required-message="Please enter your phone number.">
                                                <p class="help-block text-danger"></p>
                                            </div>
                                            <div class="form-group">
                                                <textarea class="form-control" placeholder="Your Address *" id="address" required data-validation-required-message="Please enter a message."></textarea>
                                                <p class="help-block text-danger"></p>
                                            </div>
                                            <div class="form-group">
                                                <input type="tel" class="form-control" placeholder="Pincode *" id="pincode" required data-validation-required-message="Please enter your pincode.">
                                                <p class="help-block text-danger"></p>
                                            </div>
                                            <div class="form-group">
                                                <select id="day" name="day" class="form-control form-dropdown">
                                                    <option value="today">Today</option>
                                                    <option value="tomorrow">Tomorrow</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <select id="timeslot" name="timeslot" class="form-control form-dropdown">
                                                    <option value="11">10 am to 11 am</option>
                                                    <option value="12">11 am to 12 pm</option>
                                                    <option value="13">12 pm to 1 pm</option>
                                                    <option value="14">1 pm to 2 pm</option>
                                                    <option value="15">2 pm to 3 pm</option>
                                                    <option value="16">3 pm to 4 pm</option>
                                                    <option value="17">4 pm to 5 pm</option>
                                                    <option value="18">5 pm to 6 pm</option>
                                                    <option value="19">6 pm to 7 pm</option>
                                                    <option value="20">7 pm to 8 pm</option>
                                                </select>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-lg-12 text-center">
                                                <button type="submit" class="btn btn-xl">Book</button>
                                            </div>
                                        </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer');?>