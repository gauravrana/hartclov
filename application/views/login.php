<?php $this->load->view('common/pagehead'); ?>
<script>
    function onSuccess(googleUser) {
        console.log('Logged in as: ' + googleUser.getBasicProfile().getName());
    }
    function onFailure(error) {
        console.log(error);
    }
    function renderButton() {
        gapi.signin2.render('my-signin2', {
            'scope': 'https://www.googleapis.com/auth/plus.login',
            'width': 200,
            'height': 50,
            'longtitle': true,
            'theme': 'dark',
            'onsuccess': onSuccess,
            'onfailure': onFailure
        });
    }
    function onSignIn(googleUser) {
        var profile = googleUser.getBasicProfile();
        console.log('ID: ' + profile.getId());
        console.log('Name: ' + profile.getName());
        console.log('Image URL: ' + profile.getImageUrl());
        console.log('Email: ' + profile.getEmail());
    }
    function signOut() {
        var auth2 = gapi.auth2.getAuthInstance();
        auth2.signOut().then(function () {
            console.log('User signed out.');
        });
    }
</script>
<body>
	<div id="main_container">
        <div class="g-signin2" data-onsuccess="onSignIn"></div>
		<div class="login_form" id="login">

		
			<h3>Valyoo System Manager</h3>
			
			
			<form action="/index.php/authenticate" name="adduser"  method="post" id="adduser"
					class="niceform">
			<div>
				<dl>
							<dt>
								<label for="username">User Id</label><label for ="star" style="color:red;">*</label>
							</dt>
							<dd>
								<input type="text" name="username" id="username" size="25" required="required"/>
							</dd>
							<dd>For example: example@dealskart.in</dd>
						</dl>
						<dl>
                                                        <dt>
                                                                <label for="username">Password</label><label for ="star" style="color:red;">*</label>
                                                        </dt>
                                                        <dd>
                                                                <input type="password" name="password" id="password" size="25" required="required"/>
                                                        </dd>
                                                </dl>
						<dl id="dept" style="display:none;">
                                                        <dt>
                                                                <label for="department">Department</label><label for ="star" style="color:red;">*</label>
                                                        </dt>
                                                        <dd>
                                                                <select id="selDept" name="selDept" onchange="openDepartment();"/>
								<option value=""> Please Select</option>
								</select>
								<input type="text" name="selOther" id="selOther" placeholder="Enter Department" style="display:none;"/>
                                                        </dd>
							<dt>
                                                                <label for="department">C Zentrix Dialler ID</label>
                                                        </dt>
                                                        <dd>
								<input type="text" name="cZentrix" id="cZentrix" size="25" placeholder="Optional" />
                                                        </dd>
                                                </dl>
						<dl class="submit">
							<dt></dt>
							<dd style="width: 486px;">
								<input type="submit" name="adduser" id="adduser"
									value="Submit" /> 
							</dd>
						</dl>
						<dl style="margin-left:175px;">
							<dd>
								<a href="/forgotPassword">Forgot Password</a>
							</dd>
						</dl>
						</form>
					
					
					<dl style="color: #FF4A4A;margin-left:175px;">
					<div id="showMsg"></div>
                                        <?php if (isset($message)){
                                                echo $message;
                                                } ?>
                                </dl>
                                <dl style="color:green;margin-left:150px;font-weight:bold;">
                                        <?php if (isset($_GET['gc']) && $_GET['gc']==1){
                                                echo "You have been successfully logged out!";
                                                } ?>
                                </dl>
			</div>
			
			<?php $this->load->view('common/footer'); ?>
